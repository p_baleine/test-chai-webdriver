var express = require('express');
var multiline = require('multiline');
var app = express();

app.get('/', function(req, res) {
  res.type('html');
  res.send(multiline(function() {/*
<!doctype html>
<html>
  <head></head>
  <body>
    Hello world.
    <div id="hoge">
      piyopiyo
    </div>
  </body> 
</html>
  */}));
});

module.exports = app;
