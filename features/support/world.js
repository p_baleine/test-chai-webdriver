var sw = require('selenium-webdriver');
var driver = new sw.Builder()
      .withCapabilities(sw.Capabilities.chrome())
      .build();

var chai = require('chai');
var chaiWebdriver = require('chai-webdriver');

var app = require('../../lib/app');

chai.use(chaiWebdriver(driver));

var World = function World(callback) {
  this.server = null;
  this.driver = driver;
  callback();
};

World.prototype.bootServer = function(callback) {
  this.server = app.listen(3000, callback);
};

World.prototype.shutdownServer = function(callback) {
  this.server.close(callback);
};

exports.World = World;
