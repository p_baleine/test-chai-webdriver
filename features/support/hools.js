module.exports = function() {
  this.Before(function(callback) {
    this.bootServer();
    callback();
  });

  this.After(function(callback) {
    this.shutdownServer();
    callback();
  });
};
