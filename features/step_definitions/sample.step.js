var expect = require('chai').expect;
var Promise = require('bluebird');

module.exports = function() {
  this.World = require("../support/world.js").World;

  this.Given(/^I browse to top$/, function (callback) {
    this.driver.get('http://localhost:3000/');
    callback();
  });

  this.Then(/^It returns 'Hello world'$/, function (callback) {
    this.driver.wait(function() {
      return Promise.all([
        expect('body').dom.to.contain.text('Hello world'),
        expect('#hoge').dom.to.contain.text('piyopiyo')
      ])
        .then(function() {
          callback();
        });
    }, 2000);
  });
};
